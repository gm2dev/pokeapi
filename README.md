# Tu PokeAPI

Hola!

PokeApi es una API pública no-oficial que nos provee información sobre Pokemons.
Queremos que nos ayudes a utilizarla.

## Objetivo

Tu objetivo es poder cumplir con las siguiente historia de usuario.
No debería de llevarte mas de unas horas!

```
- Como usuario, quiero poder ver un listado de Pokemons, con sus nombres, imagenes y estadísticas.
```

Optativa, solo si te copaste:
```
- Como usuario me gustaria buscar un Pokemon en particular por su nombre.
```

## Metodología

Utiliza el Framework/Librería correspondiente a la búsqueda!
Si por alguna razon te sentis mas comodo/a con otra tecnología preguntale a el/la recruiter.

El objetivo del desarrollo es que puedas trabajar con comodidad sobre el código.
Tenes la posibilidad de usar todo lo que creas necesario para llevarlo adelante
de la mejor manera posible (librerías, linters, versionadores, etc).

Anotá todas las suposiciones que necesites en el README.md

## Recursos
- https://pokeapi.co/about

